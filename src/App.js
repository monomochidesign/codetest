import React, {Component} from 'react'
import './App.css'
import Products from './product'
import Filter from './filter'
 

export default class App extends Component {
  constructor(){
    super();
    this.state={
      isLoaded: false,
      products:[],
      filteredProd:[],
      cat:''
    };
  }

  async componentDidMount(){ 
  //api fetch request within component lifecycle and setState on data retrieval
    const url = "https://api.jsonbin.io/b/5e9fc4a82940c704e1dc7893"
    const response = await fetch(url,{
          headers: { crossDomain: true, "Content-Type": "application/json" }
      })
    const data = await response.json()
    this.setState({
      isLoaded:true,
      products:data,
      filteredProd:data
    });
  }

//filtering by size, requires two state variables for filter and reset 
 filteringSize = (e) =>{
    let size = e.target.value;
    var filteredProd = this.state.products

    if(size === ''){ //resets to all products when selecting default option
       this.setState({
        cat:size,
        filteredProd:this.state.products
       })
       console.log(this.state.products);
      }
   
      else{
        this.setState({ //filters data based on dropdown option
          cat:size,
          filteredProd: filteredProd.filter(product=>{
            return product.size.indexOf(e.target.value)>=0     
          }),
          products:filteredProd
        })
         console.log(filteredProd);
      }
    }

  render (){
    var {isLoaded, products, filteredProd} = this.state;
    //check if loaded
    if(!isLoaded){
      return <div> Loading...</div>;
    } else {
      return(
        <div className="App">

          <div className="container">
            <h1>Women's top</h1>
            {/*applying product filter*/}
            <Filter filteringSize={this.filteringSize} cat={this.state.cat} />
          </div>
           {/*pulling in dynamic products*/}
          <Products products={filteredProd? filteredProd: this.state.products}/>
        </div>
      )
    }
  } 
 }    
  


