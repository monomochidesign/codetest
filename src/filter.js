import React from 'react'

export default function Filter({size,filteringSize}){
  return(
    <div className="filter">
      <div className="selectFilter">
       {/*creating option values and setting onChange functions*/}
        <select value={size} onChange={filteringSize}>
        <option value="">Filter by size</option>
          <option value="XS">XS</option>
          <option value="S">S</option>
          <option value="M">M</option>
          <option value="L">L</option>
          <option value="XL">XL</option>
        </select>
      </div>
    </div>
    )
}