import React from 'react'

export default function Product(props){
  const {product} =props;
    return (
      <div className="productList">
        {/* Accessing product property to return elements from JSON data*/}
          <div key={product.productName} className="item">

            <img src={product.productImage} alt={product.productName}/>
            {/*Boolean expression to check if product has flags */}
            <div className="flagContainer">
              {product.isSale && <div className="onSale">Sale</div>}
              {product.isExclusive && <div className="isEx">Exclusive</div>}
            </div>
            <div className="productDesc">
              <p className="productName">{product.productName}</p>
              <p className="productPrice">{product.price}</p>
            </div>
          </div>
      </div>
    )
 
}

