import React from 'react'
import Product from './productList'

export default function Products(props){
  return(
    <div className="products">
	{/*product mapping and assigning individual key to items*/}
    {props.products.map(product=>(
        <Product key={product.index} product={product} />
      ))}
    </div>
    )
}