Instructions on how to use the repository.

1. Clone or download repository as a zip file
2. Before proceeding with the below, please make sure you have npm and node.js installed on your machine.
3. Open Terminal on your machine
4. Type: cd my-app
5. Type: npm run
6. Your browser should open window accessing: http://localhost:3000/ to view the app
